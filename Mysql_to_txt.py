import pymysql.cursors
import os
import re
import time

# Variables
mysql = pymysql.connect(
    host='localhost',
    user='test',
    password='password',
    db='test'
)

def write_to_file(text):
    f = open("out.txt", "a")
    f.write(str(text) + "\n")
    f.close()

def Mysql_get():
    try:
        with mysql.cursor() as cursor:
            # Read a single record
            mysql_data = "SELECT * FROM `User`"
            cursor.execute(mysql_data)
            result = cursor.fetchall()
            print("(ID, Username)")
            for column in result:
                print(column)
                write_to_file(column)
    finally:
        mysql.close()

Mysql_get()